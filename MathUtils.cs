﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise8_BasicC__StaticClass
{
    public static class MathUtils
    {
        public static int Cong(int a, int b)
        {
            return a + b;
        }

        public static int Tru(int a, int b)
        {
            return a - b;
        }

        public static int Nhan(int a, int b)
        {
            return a * b;
        }

        public static int Chia(int a, int b)
        {
            return a / b;
        }

        public static int LuyThua(int a, int b)
        {
            return (int)Math.Pow(a, b);
        }

        public static void Menu()
        {
            Console.WriteLine("Cho phep toan: ");
            Console.WriteLine("1. Cong ");
            Console.WriteLine("2. Tru ");
            Console.WriteLine("3. Nhan ");
            Console.WriteLine("4. Chia ");
            Console.WriteLine("5. Luy thua ");
            Console.WriteLine("Nhap phep tinh tuong ung: ");
        }
    }
}
