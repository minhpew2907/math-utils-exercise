﻿

using Exercise8_BasicC__StaticClass;

int choice = 0;
do
{
    MathUtils.Menu();
    choice = int.Parse(Console.ReadLine());
    Console.Clear();

    Console.WriteLine("Nhap so thu nhat: ");
    int firstNumber = int.Parse(Console.ReadLine());
    Console.WriteLine("Nhap so thu hai");
    int secondNumber = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            Console.WriteLine($"Ket qua: {MathUtils.Cong(firstNumber, secondNumber)}");
            break;
        case 2:
            Console.WriteLine($"Ket qua: {MathUtils.Tru(firstNumber, secondNumber)}");
            break;
        case 3:
            Console.WriteLine($"Ket qua: {MathUtils.Nhan(firstNumber, secondNumber)}");

            break;
        case 4:
            Console.WriteLine($"Ket qua: {MathUtils.Chia(firstNumber, secondNumber)}");

            break;
        case 5:
            Console.WriteLine($"Ket qua: {MathUtils.LuyThua(firstNumber, secondNumber)}");

            break;
        case 6:
            Console.WriteLine("Thank you! Good bye");
            Environment.Exit(0);
            break;
        default:
            break;
    }
} while (choice != 5);